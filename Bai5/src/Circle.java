import java.util.Scanner;

public class Circle extends Geometry{
    private Double radius;

    public Circle(){
        super();
    }

    public Circle(Double r){
        super();
        this.radius = r;
    }

    @Override 
    public void ImportValue(){
        System.out.println("Please import radius for circle!");
        Scanner sc = new Scanner(System.in);
        this.radius= sc.nextDouble();
    }

    @Override
    public double Area(){
        return Math.PI * Math.pow(radius,2);
    }

    @Override
    public double Perimeter(){
        return Math.PI * 2* radius;
    }

    public void show(){
        String x = "Hình tròn bán kính " + radius + " có chu vi: " + Perimeter() + " và diện tích: " + Area();
        System.out.println(x);
    }
}
