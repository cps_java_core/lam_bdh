public class Class extends Faculty{
    public int schoolYear;
    public Class() {
        super();
    }

    public Class(String id,int sy) {
        this.id = id;
        this.schoolYear = sy;
    }

    public String getInfo(){
        return this.id +"K"+ String.valueOf(this.schoolYear);
    }
}
