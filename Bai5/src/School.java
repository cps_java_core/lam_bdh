public class School {
    public String name;
    public String shortName;
    public Faculty faculty;
    public String id;
    //overloading - quá tải hàm
    public School() {
        
    }

    public School(String name) {
        this.name = name;
        this.shortName = null;
    }
    
    public School(String name,String sn) {
        this.name = name;
        this.shortName = sn;
    }

    // public String getDomain(){
    //     return this.shortName + ".edu.vn";
    // }

    public String getSchool(){
        return this.name; 
    }
}
