import java.sql.Date;

public class Student extends People{
    public String sID;
    public School school;
    public Faculty faculty;
    public Class studentClass;
    //public String email;
    public String getSchool(){
        return school.getSchool();
    }

    public String getFaculty(){
        return faculty.getName();
    }

    public String getStudentClass(){
        return studentClass.getInfo();
    }
    public Student() {
        
    }

    public Student(String sID) {
        
    }

    public Student(School school) {
        
    }

    public Student(String sID,String name){
        this.sID = sID;
        this.name = name;
    }

    public Student(String sID,String name,Gender gender,Date birthDay,String id,ColorHair colorHair,Skin colorSkin,School school,Faculty faculty,Class sc){
        this.sID = sID;
        this.name = name;
        this.gender = gender;
        this.birthDay = birthDay;
        this.id = id;
        this.colorHair = colorHair;
        this.colorSkin = colorSkin;
        this.school = school;
        this.faculty = faculty;
        this.studentClass = sc;
    }

    public String getEmail(){
        return this.sID + "@" + this.school.shortName + ".edu.vn";
    }

    public void test(){
        School dlu = new School();
        School ictu = new School("Đại học thái nguyên");
        School fpt = new School("Đại học FPT","FPT");
        School cps; //Không được khởi tạo do không được gán vào vùng nhớ nào => null
    }

    @Override
    public void talk(){
        super.talk();
        System.out.println("English");
    }

    public void show(){
        System.out.println(this.sID + "\t" +this.name+"\t"+this.gender + "\t"+this.birthDay 
        + "\t"+this.school.getSchool() +"\t"+ this.faculty.getName()+"\t" 
        + this.studentClass.getInfo()+ "\t"+ getEmail() + "\t"+this.id + "\t"+this.colorHair + "\t"+this.colorSkin);
    }
}
