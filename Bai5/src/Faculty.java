public class Faculty {
    public String name;
    public String id;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setID(String id){
        this.id = id;
    }

    public String getID(){
        return id;
    }

    public Faculty() {
        
    }

    public Faculty(String name) {
        this.name = name;
    }

    public Faculty(String id,String name) {
        this.id = id;
        this.name = name;
    }
}
