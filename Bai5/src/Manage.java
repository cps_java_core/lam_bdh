import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Console;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Manage {
    //Không giới hạn trường tìm kiếm
    public static List<Student> students = new ArrayList<Student>();
    SimpleDateFormat fDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    
    public Integer FindFaculty(){
        Integer count =0;
        // Tìm kiếm sinh viên theo trường nhập vào
        for(Student item : students){
            if (item.school.id == "IT")
                return count++;
        }
        return count;
    }

    public void AddToList(Student s){
        students.add(s);
    }

    public void AddStudent(){        
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter numbers of students:");
        int n = Integer.parseInt(sc.nextLine());
        for(int i = 0;i<n;i++){
            System.out.println("Please enter information for student no"+(i+1)+":\nPlease enter ID student:");
            
            String sid = sc.nextLine();
            
            System.out.println("Please enter full name:");
            String name = sc.nextLine();
            System.out.println("Please enter gender:");
            String gen = sc.nextLine();
            Gender gender = Gender.valueOf(gen);
            System.out.println("Please enter birthday:");
            String d = sc.nextLine();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date birthDay = Date.valueOf(d);
            formatter.format(birthDay);
            System.out.println("Please enter identity card numbers:");
            String id = sc.nextLine();
            System.out.println("Please enter color hair:");
            String ch = sc.nextLine();
            ColorHair colorHair = ColorHair.valueOf(ch);
            System.out.println("Please enter color skin:");
            String s = sc.nextLine();
            Skin colorSkin = Skin.valueOf(s);
            System.out.println("Please enter School name:");
            String sn = sc.nextLine();
            System.out.println("Please enter School short name:");
            String ssn = sc.nextLine();
            School school = new School(sn,ssn);
            System.out.println("Please enter Faculty name:");
            String fn = sc.nextLine();
            System.out.println("Please enter Faculty short name:");
            String fsn = sc.nextLine();
            System.out.println("Please enter school year:");
            int sy = sc.nextInt();
            Faculty faculty = new Faculty(fn);
            Class sClass  =new Class(fsn,sy);
            AddToList(new Student(sid,name,gender,birthDay,id,colorHair,colorSkin,school,faculty,sClass));
        sc.close();
        }
    }

    public List<Student> InputFile(){
        Student st = new Student();
        try {
            File fi = new File("D:/Java/Bai5/bai1.txt");
            BufferedReader bis = new BufferedReader(new FileReader(fi));
            
            String line = bis.readLine();
            String[] words = line.split("\t");
            Student s = new Student();
            s.sID = words[0];
            s.name = words[1];
            s.gender = Gender.valueOf(words[2]);
            s.birthDay = Date.valueOf(words[3]);
            s.id = words[8];
            s.colorHair = ColorHair.valueOf(words[9]);
            s.colorSkin = Skin.valueOf(words[10]);
                s.school = new School(words[4],words[7].replace(words[0] +"@","").replace(".edu.vn", ""));
                s.faculty = new Faculty(words[5]);
                s.studentClass = new Class(words[6].substring(0,2),Integer.valueOf(words[6].substring(3)));
                students.add(s);

            while(line != null){
                //System.out.println(line);
                line = bis.readLine();
                words = line.split("\t");
                s = new Student();
                s.sID = words[0];
                s.name = words[1];
                s.gender = Gender.valueOf(words[2]);
                s.birthDay = Date.valueOf(words[3]);
                s.id = words[8];
                s.colorHair = ColorHair.valueOf(words[9]);
                s.colorSkin = Skin.valueOf(words[10]);
                s.school = new School(words[4],words[7].replace(words[0] +"@","").replace(".edu.vn", ""));
                s.faculty = new Faculty(words[5]);
                s.studentClass = new Class(words[6].substring(0,2),Integer.valueOf(words[6].substring(3)));
                students.add(s);
            }
                
                
           
            // System.out.println(s.sID + "\t" +s.name+"\t"+s.gender + "\t"+s.birthDay 
            // + "\t"+s.school.getSchool() +"\t"+ s.faculty.getName()+"\t" 
            // + s.studentClass.getInfo()+"\t"+s.id + "\t"+s.colorHair + "\t"+s.colorSkin);
            
            bis.close();
        
        } catch (Exception e) {
            System.out.println("Error!");
        }
        return students;
    }

    public void OutputFile(){
        try {
            FileOutputStream f = new FileOutputStream("D:/Java/Bai5/bai2.txt");
            ObjectOutputStream oos = new ObjectOutputStream(f);

            for(Student item : students){
                oos.writeObject(item.sID + "\t" +item.name+"\t"+item.gender + "\t"+item.birthDay 
                + "\t"+item.school.getSchool() +"\t"+ item.faculty.getName()+"\t" 
                + item.studentClass.getInfo() +"\t"+item.getEmail() +"\t" +item.id + "\t"+item.colorHair + "\t"+item.colorSkin);
            }
            f.close();
            oos.close();
        } catch (Exception e) {
            System.out.println("Error!");
        }
    }

    public void ExportStudent(){
        System.out.println("Your list student:");
        for(Student item : students){
            item.show();
        }
    }

    public List<Student> DeleteStudent(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter ID of student you wanna delete!");
        String sID = sc.nextLine();
        for(Student item : students){
            if(item.sID.equals(sID))
                students.remove(item);
        }
        return students;
    }

    public List<Student> EditStudent(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter ID of student you wanna edit!");
        String sID = sc.nextLine();
        for(Student item : students){
            if(item.sID.equals(sID))
                {
                    System.out.println("Please enter ID student:");
                    String sid = sc.nextLine();
                    System.out.println("Please enter full name:");
                    String name = sc.nextLine();
                    System.out.println("Please enter gender:");
                    String gen = sc.nextLine();
                    Gender gender = Gender.valueOf(gen);
                    System.out.println("Please enter birthday:");
                    String d = sc.nextLine();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    Date birthDay = Date.valueOf(d);
                    formatter.format(birthDay);
                    System.out.println("Please enter identity card numbers:");
                    String id = sc.nextLine();
                    System.out.println("Please enter color hair:");
                    String ch = sc.nextLine();
                    ColorHair colorHair = ColorHair.valueOf(ch);
                    System.out.println("Please enter color skin:");
                    String s = sc.nextLine();
                    Skin colorSkin = Skin.valueOf(s);
                    System.out.println("Please enter School name:");
                    String sn = sc.nextLine();
                    System.out.println("Please enter School short name:");
                    String ssn = sc.nextLine();
                    School school = new School(sn,ssn);
                    System.out.println("Please enter Faculty name:");
                    String fn = sc.nextLine();
                    System.out.println("Please enter Faculty short name:");
                    String fsn = sc.nextLine();
                    System.out.println("Please enter school year:");
                    int sy = sc.nextInt();
                    Faculty faculty = new Faculty(fn);
                    Class sClass  =new Class(fsn,sy);
                    item.sID = sid;
                    item.name = name;
                    item.gender = gender;
                    item.birthDay = birthDay;
                    item.colorHair = colorHair;
                    item.colorSkin = colorSkin;
                    item.faculty = faculty;
                    item.school = school;
                    item.studentClass = sClass;
                    item.id = id;
                    
                }
        }
        return students;
    }

    public void findBySID(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter ID of student you wanna edit!");
        String sID = sc.nextLine();
        for(Student item : students){
            if(item.sID.equals(sID))
                {
                    System.out.println("Student you wanna find is:");
                    item.show();
                }
        }
    }

    public void findByName(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter ID of student you wanna edit!");
        String name = sc.nextLine();
        for(Student item : students){
            if(item.name.equalsIgnoreCase(name))
            {
                System.out.println("Student you wanna find is:");
                item.show();
            }
        }
    }
    
    public void geometricInput(){
        System.out.println("Please import value for geometric (up to 3 values, enter exit if u see enough):");
        Scanner sc = new Scanner(System.in);
        Double value;
        int count =0;
        String i;
        Double[]arr = new Double[4];
        while(true){
            System.out.println("Nhập 1 số nguyên dương!");
            if(arr[3] != null )
                    break;
            i = sc.nextLine();
            if(i.equalsIgnoreCase("exit"))
                break;
            else
            try {
                
                value = Double.parseDouble(i);
               
                    if(value>0){
                        arr[count++] = value;
                    }       
                    else
                        System.out.println("Nhập sai, vui lòng nhập lại 1 số nguyên dương!");
                
            } catch (Exception e) {
                System.out.println("Nhập sai, vui lòng nhập lại 1 số nguyên dương!");
            }
            
        }
        if(arr[3]!=null){
            if(arr[0] < arr[1]){
                if(arr[1] < arr[2]+arr[3] || arr[3] < arr[1] + arr[2] || arr[2] < arr[3] + arr[1]){
                    Circle s = new Circle(arr[0]/2);
                    Square q = new Square(arr[2],arr[2]);
                    Rectangle r = new Rectangle(arr[0],arr[1]);
                    Triangle t = new Triangle(arr[1],arr[2],arr[3]);
                    Double a = s.Area()/2 + q.Area()+r.Area()+t.Area();
                    Double p = s.Perimeter()/2 + q.Perimeter()*3/4 + r.Perimeter()/2 + arr[3];
                    s.show();q.show();r.show();t.show();
                    System.out.println("Area of the land: " +a + " and Perimeter of the land: " + p);
                }
                else 
                    System.out.println("Nhập sai dữ liệu b,c,d!");
            }
            else 
                System.out.println("Nhập sai dữ liệu a và b!");
        }

        else if(arr[1] == null){
            System.out.println("Circle!");
            Circle s = new Circle(arr[0]);
            s.show();
        } 
        else if(arr[0] == arr[1] && arr[2]==null){
            System.out.println("Square!");
            Square q = new Square(arr[0],arr[1]);
            q.show();
        }
        else if(arr[2] == null){
            System.out.println("Rectangle!");
            Rectangle r = new Rectangle(arr[0],arr[1]);
            r.show();
        }
        else if(arr[1] < arr[2]+arr[0] || arr[0] < arr[2] + arr[1] || arr[2] < arr[1] + arr[0]){
            System.out.println("Triangle!");
            Triangle t = new Triangle(arr[0],arr[1],arr[2]);
            t.show();
        }
        else{
            System.out.println("Import value error! Please try again!");
        }
    }
}

