public abstract class Geometry {
    public abstract void ImportValue();
    public abstract double Perimeter();
    public abstract double Area();
}


