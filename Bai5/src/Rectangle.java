import java.util.Scanner;

public class Rectangle extends Geometry{
    private Double length;
    private Double width;

    public Rectangle(){
        super();
    }

    public Rectangle(Double l,Double w){
        super();
        this.length = l;
        this.width = w;
    }

    @Override 
    public void ImportValue(){
        System.out.println("Please import size for Rectangle!");
        Scanner sc = new Scanner(System.in);
        this.length= sc.nextDouble();
        this.width= sc.nextDouble();
    }

    @Override
    public double Area(){
        return length * width;
    }

    @Override
    public double Perimeter(){
        return (length + width) *2;
    }

    public void show(){
        String x = "Hình chữ nhật chiều rộng: "+ width + ", chiều dài: "+length + " có chu vi: " + Perimeter() + " và diện tích: " + Area();
        System.out.println(x);
    }
}
