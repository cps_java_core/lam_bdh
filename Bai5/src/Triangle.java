import java.util.Scanner;

public class Triangle extends Geometry{
    private Double Fedge;
    private Double Sedge;
    private Double Tedge;


    public Triangle(){
        super();
    }

    public Triangle(Double f, Double s,Double t){
        super();
        this.Fedge = f;
        this.Sedge = s;
        this.Tedge = t;
    }

    @Override 
    public void ImportValue(){
        System.out.println("Please import radius for Triangle!");
        Scanner sc = new Scanner(System.in);
        this.Fedge= sc.nextDouble();
        this.Sedge = sc.nextDouble();
        this.Tedge = sc.nextDouble();
    }

    @Override
    public double Area(){
        return Math.sqrt((Fedge + Sedge + Tedge)*(Fedge + Sedge -Tedge)*(Fedge+Tedge-Sedge)*(Sedge + Tedge-Fedge))/4;
    }

    @Override
    public double Perimeter(){
        return Fedge + Sedge + Tedge;
    }

    public void show(){
        String x = "Hình tam giác có các cạnh lần lượt: " + Fedge + ", "+Sedge+ ", "+Tedge+ " có chu vi: " + Perimeter() + " và diện tích: " + Area();
        System.out.println(x);
    }
}
