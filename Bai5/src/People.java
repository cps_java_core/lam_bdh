import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class People {
    public String name;
    public Integer age;
    public Gender gender;
    public Skin colorSkin;
    public ColorHair colorHair;
    public String id;
    public String info;
    public Date birthDay;

    public void setID(String id){
        this.id = id;
    }

    //Thuộc tính viết theo kiểu hàm
    public String getID(){
        if(this.age >= 13){
            return this.id;
        }
        return null;
    }

    public void setAge(Integer age){
        this.age = age;
    }

    public Integer getAge(){
        return age;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setBirthDay(Date birthDay){
        
        this.birthDay =birthDay;
    }

    public Date getBirthDay(){
        return birthDay;
    }

    public Gender getGender(){
        return gender;
    }

    public void setGender(Gender gender){
        this.gender = gender;
    }

    public void setSkin(Skin colorSkin){
        this.colorSkin = colorSkin;
    }

    public Skin getSkin(){
        return colorSkin;
    }
    
    public void talk(){
        System.out.println("Tiếng mẹ đẻ");
    }

    public String info(){
        return this.getName() + this.gender + this.getBirthDay() + this.getID() + this.colorHair + this.colorSkin;
    }
    
}

enum Gender{
    male,female,other
}

enum ColorHair{
    brown,blonde,black,red,auburn,gray,white
}

enum Skin{
    light,dark
}
