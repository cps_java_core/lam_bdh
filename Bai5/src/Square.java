import java.util.Scanner;

public class Square extends Geometry{
    private Double edge1;
    private Double edge2;

    public Square(){
        super();
    }

    public Square(Double e1,Double e2){
        super();
        this.edge1 = e1;
        this.edge2 = e2;
    }

    @Override 
    public void ImportValue(){
        System.out.println("Please import edge for Square!");
        Scanner sc = new Scanner(System.in);
        this.edge1= sc.nextDouble();
        this.edge2= sc.nextDouble();
    }

    @Override
    public double Area(){
        return Math.pow(edge1, 2);
    }

    @Override
    public double Perimeter(){
        return edge1*4;
    }

    public void show(){
        String x = "Hình vuông cạnh " + edge1 + " có chu vi: " + Perimeter() + " và diện tích: " + Area();
        System.out.println(x);
    }
}
