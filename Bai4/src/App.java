
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class App {
    public static void main(String[] args) throws Exception {
        /*
        System.out.println("Entered integer: " + n);
        System.out.println( Cau2(n));
        
        Cau3_ImportEdge();
        int n = InputN();
        System.out.println(Cau22(n));
        Cau5()*/

        Cau1_FirstChar();
    }

    public static int InputN(){
        int n;
        Scanner sc = new Scanner(System.in);
        
        while(true){
            String s = sc.nextLine();
            try {
                //System.out.println("Please import a integer!");
                n = Integer.parseInt(s);
                if(n >= 0)
                    break;
                else
                    System.out.println("Error, please import again!");
            } catch (Exception e) {
                System.out.println("Error, please import again!");
            }
        }
        
        return n;
    }

    public static double Cau1(int n){
        double result =1;
        if(n<2)
            return 1;
            for(int i = 2;i<=n;i++){
                
                result += (double)1/(i*3);
            }
        return Math.round(result*1000.0)/1000.0;
        
    }
    
    public static double Cau11(int n){
        if(n<2)
            return 1;
        
        return Math.round(((double)1/(n*3)+Cau11(n-1)) *1000.0)/1000.0;   
    }

    public static double Cau2(int n){
        double e=1;
        
        for(int i=0;i<=n;i++){
            e+= e* (double)(2*(i+1)/(double)(2*i+3));
            //e+=d;
        }
        return e;
    }

    public static double Cau22(int n){
        double d = 1;
        if(n<0)
            return 1;
        for(int i = 0;i<=n;i++)
            d*= (double)2*(i+1) /(double)(2*i+3);
        
        return  d + Cau22(n-1);
    }

    public static void Cau3_ImportEdge(){
        int x,y,z;
        System.out.println("Import 3 edges of a triangle: ");
        System.out.println("Import first edge: ");
        x = InputN();
        System.out.println("Import second edge: ");
        y = InputN();
        System.out.println("Import third edge: ");
        z = InputN();
        System.out.println("Values of three edges: " + x + "\t" + y + "\t" + z + "\n");   
        
        if(x>=y+z || y>=z+x || z>=x+y || x == 0 || y== 0 || z == 0)
            System.out.println("x,y,z are not edges of a triangle!");

        else if(x==y && x==z)
            System.out.println("Equilateral triangle!");
        else{
            if(x!=y && x!=z && y!=z){
                if(x*x+y*y==z*z ||x*x+z*z==y*y|| z*z+y*y==x*x)
                    System.out.println("Right triangle!");
                else if(x*x+y*y<z*z ||x*x+z*z<y*y|| z*z+y*y<x*x)
                    System.out.println("Obtuse triangle!");
                else
                    System.out.println("Scalene triangle!");
            }
                
            else
                System.out.println("Isosceles triangle!");
        }   
        double s = (double)1/4*Math.sqrt((x+y+z)*(x+y-z)*(y+z-x)*(x+z-y));
        System.out.println("Area: "+ s);
    }

    public static double ImportDouble(){
        double d;
        Scanner sc = new Scanner(System.in);
        while(true){
            try {
                System.out.println("Please import a positive double number!");
                String s = sc.nextLine();
                d = Double.parseDouble(s);
                if(d>0)
                    break;
                else
                    System.out.println("Value is not valid. Please import again!");
            } catch (Exception e) {
                //TODO: handle exception
                System.out.println("Value is not valid. Please import again!");
            }
        }
        return d;  
    }

    public static void Cau5(){
        //lower case, upper case, symbol, ràng buộc.
        Map<String,List<Number>> map = value();
        List<Double> ld = new ArrayList<>();
        double a;
        int i =0;
        double [][]arr = new double[9][9];
        int serial = 0;
        String s ="";
        Scanner sc = new Scanner(System.in);
        System.out.println("There are units can be exchanged:");
        for(Map.Entry<String,List<Number>> entry : map.entrySet()){
            System.out.print(entry.getKey() + '\t');
        }
        while(true){
            
            System.out.println("\nPlease import currency unit!");
            s = sc.nextLine();
            s = s.toUpperCase();
            
                if(s.equalsIgnoreCase("USD") 
                    || s.equalsIgnoreCase("EUR")
                    || s.equalsIgnoreCase("GBP")
                    || s.equalsIgnoreCase("INR")
                    || s.equalsIgnoreCase("AUD")
                    || s.equalsIgnoreCase("CAD")
                    || s.equalsIgnoreCase("ZAR")
                    || s.equalsIgnoreCase("NZD")
                    || s.equalsIgnoreCase("JPY")
                    || s.equalsIgnoreCase("VND")
                ){ 
                    break;
                }
                else
                    System.out.println("Error, please import like item array provided!");
            
        }
        double d = ImportDouble();
        System.out.println("Value just entered is: " + d + " " +s);

        //System.out.println(serial);

        for(Map.Entry<String,List<Number>> entry : map.entrySet()){ 
            switch(s.toUpperCase()){
                case "USD":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(0).doubleValue() * d * 100000.0)/100000.0);
                case "EUR":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(1).doubleValue() * d * 100000.0)/100000.0);
                case "GBP":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(2).doubleValue() * d * 100000.0)/100000.0);
                case "INR":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(3).doubleValue() * d * 100000.0)/100000.0);
                case "AUD":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(4).doubleValue() * d * 100000.0)/100000.0);
                case "CAD":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(5).doubleValue() * d * 100000.0)/100000.0);
                case "ZAR":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(6).doubleValue() * d * 100000.0)/100000.0);
                case "NZD":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(7).doubleValue() * d * 100000.0)/100000.0);
                case "JPY":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(8).doubleValue() * d * 100000.0)/100000.0);
                case "VND":
                    System.out.println(entry.getKey() + ": " + Math.round(entry.getValue().get(9).doubleValue() * d * 100000.0)/100000.0);
            }
                
            
            
        }
        // for(int i=0;i<Unit.length;i++){
        //     if(i != serial)
        //     System.out.println(Unit[i] + ": " + Math.round(d*Ratio[serial][i]*100000.0)/100000.0);
        // }
            
    }

    private static String[] Unit = {
        "USD","EUR","GBP","INR","AUD",
        "CAD","ZAR","NZD","JPY","VND"
    };

    private static double[][] Ratio = {
        { 1	,   0.80518,	0.6407,	    63.3318,	1.21828,	1.16236,	11.7129,	1.2931,	    118.337,	21385.7},
        {1.24172,	1,  	0.79575,	78.6084,	1.51266,	1.44314,	14.5371,	1.60576,	146.927,	26561.8},
        {1.56044,	1.25667,	1,  	98.7848,	1.90091,	1.81355,	18.2683,	2.01791,	184.638,	33374.9},
        {0.0158,	0.01272,	0.01012,	1,	    0.01924,	0.01836,	0.18493,	0.02043,	1.8691, 	337.811},
        {0.82114,	0.66119,	0.5262, 	52.086,	    1,  	0.95416,	9.61148,	1.06158,	97.112, 	17567.9},
        {0.86059,	0.69296,	0.55148,	54.5885,	1.04804,	1,  	10.0732,	1.11258,	101.777,	18401.7},
        {0.08541,	0.06877,	0.05473,	5.40852,	0.10398,	0.09924,	1,  	0.11037,	10.0996,	1825.87},
        {0.77402,	0.62319,	0.49597,	49.0031,	0.94215,	0.89951,	9.06754,	1,  	91.5139,	16552.1},
        {0.00846,	0.00681,	0.00542,	0.53547,	0.0103, 	0.00983,	0.09908,	0.01093,	1,  	180.837},
        {0.00005,	0.00004,	0.00003,	0.00296,	0.00006,	0.00005,	0.00055,	0.00006,     0.00553,   1}

    };

    private static Map value(){
        // List<Number> USDValue = Arrays.asList(1	,   0.80518,	0.6407,	    63.3318,	1.21828,	1.16236,	11.7129,	1.2931,	    118.337,	21385.7);
        // List<Number> EURValue = Arrays.asList(1.24172,	1,  	0.79575,	78.6084,	1.51266,	1.44314,	14.5371,	1.60576,	146.927,	26561.8);
        // List<Number> GBPValue = Arrays.asList(1.56044,	1.25667,	1,  	98.7848,	1.90091,	1.81355,	18.2683,	2.01791,	184.638,	33374.9);
        // List<Number> INRValue = Arrays.asList(0.0158,	0.01272,	0.01012,	1,	    0.01924,	0.01836,	0.18493,	0.02043,	1.8691, 	337.811);
        // List<Number> AUDValue = Arrays.asList(0.82114,	0.66119,	0.5262, 	52.086,	    1,  	0.95416,	9.61148,	1.06158,	97.112, 	17567.9);
        // List<Number> CADValue = Arrays.asList(0.86059,	0.69296,	0.55148,	54.5885,	1.04804,	1,  	10.0732,	1.11258,	101.777,	18401.7);
        // List<Number> ZARValue = Arrays.asList(0.08541,	0.06877,	0.05473,	5.40852,	0.10398,	0.09924,	1,  	0.11037,	10.0996,	1825.87);
        // List<Number> NZDValue = Arrays.asList(0.77402,	0.62319,	0.49597,	49.0031,	0.94215,	0.89951,	9.06754,	1,  	91.5139,	16552.1);
        // List<Number> JPYValue = Arrays.asList(0.00846,	0.00681,	0.00542,	0.53547,	0.0103, 	0.00983,	0.09908,	0.01093,	1,  	180.837);
        // List<Number> VNDValue = Arrays.asList(0.00005,	0.00004,	0.00003,	0.00296,	0.00006,	0.00005,	0.00055,	0.00006,     0.00553,   1);

        List<Number> USDValue = Arrays.asList(1,        1.24172,    1.56044,    0.0158,	    0.82114,	0.86059,	0.08541,    0.77402,    0.00846,	0.00005);
        List<Number> EURValue = Arrays.asList(0.80518,	1,  	    1.25667,	0.01272,	0.66119,	0.69296,	0.06877,	0.62319,	0.00681,	0.00004);
        List<Number> GBPValue = Arrays.asList(0.6407,	0.79575,	1,  	    0.01012,	0.5262, 	0.55148,	0.05473,	0.49597,	0.00542,	0.00003);
        List<Number> INRValue = Arrays.asList(63.3318,	78.6084,	98.7848,	1,	        52.086,	    54.5885,	5.40852,	49.0031,	0.53547,	0.00296);
        List<Number> AUDValue = Arrays.asList(1.21828,	1.51266,	1.90091,	0.01924,	1,  	    1.04804,	0.10398,	0.94215,	0.0103, 	0.00006);
        List<Number> CADValue = Arrays.asList(1.16236,	1.44314,	1.81355,	0.01836,	0.95416,	1,  	    0.09924,	0.89951,	0.00983,	0.00005);
        List<Number> ZARValue = Arrays.asList(11.7129,	14.5371,	18.2683,	0.18493,	9.61148,	10.0732,	1,  	    9.06754,	0.09908,	0.00055);
        List<Number> NZDValue = Arrays.asList(1.2931,	1.60576,	2.01791,	0.02043,	1.06158,	1.11258,	0.11037,	1,  	    0.01093,	0.00006);
        List<Number> JPYValue = Arrays.asList(118.337,	146.927,	184.638,	1.8691, 	97.112, 	101.777,	10.0996,	91.5139,	1,  	    0.00553);
        List<Number> VNDValue = Arrays.asList(21385.7,	26561.8,	33374.9,	337.811,	17567.9,	18401.7,	1825.87,	16552.1,	180.837,	1);

        Map<String,List<Number>> map = new HashMap<>();
        map.put("USD", USDValue);
        map.put("EUR", EURValue);
        map.put("GBP", GBPValue);
        map.put("INR", INRValue);
        map.put("AUD", AUDValue);
        map.put("CAD", CADValue);
        map.put("ZAR", ZARValue);
        map.put("NZD", NZDValue);
        map.put("JPY", JPYValue);
        map.put("VND", VNDValue);
        
        return map;
    }

    private static void Cau1_FirstChar() {
        List<String> ls = new ArrayList<>();
        List<String> rs = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String input;
        Integer count = 0;
        System.out.println("Import items of array, enter \"exit\" to finish! ");
        while(true){
            input = sc.nextLine();
            if(input.equalsIgnoreCase("exit"))
                break;
            else{
                ls.add(input);
            }
        }
        System.out.println("Values of array: " + ls);
        System.out.println("Import a char: ");
        String c = sc.nextLine();
        
        for(String item : ls){
            if(c.substring(0,1).equals(item.substring(0,1))){
                count++;
                rs.add(item);
            }
        }
        if(count<1)
            System.out.println("There are nothing start by this char! ");
        else if(count == 1){
            System.out.println(count+" letter starts by this char and his value is:");
            System.out.println(rs);
        }
        else{
            System.out.println(count+" letters starts by this char and their values are:");
            System.out.println(rs);
        }
    }

    private static void StringToArray(){
        List<String> ls = new ArrayList<>();
        String s = "Hello_I_am_Iron_Man!";
        ls.add(s);
        ls.toArray();
        
        System.out.println(ls);
    }

    private static void ArrayToString(){
        List<String> ls = Arrays.asList("Hello","I","am","Iron","Man!");
        ls.toString();
        String s="";
        /*Iterator <String> iterator = ls.iterator();
        while(iterator.hasNext())
        System.out.print((String)iterator.next() + "_");*/

        for(String item : ls){
            s += item +"_";
        }
        System.out.println(s.substring(0,s.length()-1));
    }

    private static char[] Cau2_StringToArray(){
         
        String s = "Hello_I_am_Iron_Man!";

        char[] arr = s.toCharArray();
        for(char i : arr){
            System.out.println(i + "\t");
        }
        return arr;
    }

    private static String[] Cau2_C2() {
        String s = "Hello_I_am_Iron_Man!";

        String[] arr = s.split("_");
        for(String item : arr){
            System.out.println(item + '\t');
        }
        return arr;
    }

    private static String Cau3_ArrayToString() {
        char[] arr = Cau2_StringToArray();
        String s = String.valueOf(arr);
        
        System.out.println(s);
        return s;
    }

    private static String Cau3_C2() {
        String[] arr = Cau2_C2();
        String s  ="";
        for(String item : arr){
            s += item + "_";
        }
        
        System.out.println(s.substring(0,s.length()-1));
        return s;
    }

    private static void Cau4_HashMap() {
        Map<String,String> hMap = new HashMap<>();
        hMap.put("alo","1234");
        hMap.put("ola","4321");
        hMap.put("volume","volume si");
        System.out.println(hMap);
        Set<Map.Entry<String,String>> seths = hMap.entrySet();
        for(Map.Entry<String,String>i :seths){
            System.out.println(i.getKey() + " -> " + i.getValue());
        }
    }

    private static void Cau5_HashMap(){
        
        
    }
}

