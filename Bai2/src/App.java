import java.util.Scanner;


//Tư duy tách thành các lớp của em rất tốt. 
class Hi1{ 
    String x = "HI ALL! WELCOME TO CODE P SOFT GROUP!";
    // Câu 1 là viết hàm in ra tthì em hổng cần phải làm như này! 
    /// Nhớ rằng constructor củng là hàm nhưng là hàm đặt biệt. 
    public Hi1(){
        System.out.println("    Kết quả câu 1: ");
        System.out.println(x);
    }
    
}

class Calculator2{
    public Calculator2(Double x, Double y,String z){
        /*System.out.println("    Kết quả câu 2: ");
        System.out.println("Tổng của 2 số: " + (x+y));
        System.out.println("Hiệu của 2 số: " + (x-y));
        System.out.println("Tích của 2 số: " + (x*y));
        System.out.println("Thương của 2 số: " + (x/y));*/
        // Nếu kết z không phải là một phép toán thì sẽ như thế nào?
        // switch case phải cần có giá trị default không em?
        switch (z) {
            case "+": System.out.println("Tổng của 2 số: " + (x+y)); break;
            case "-": System.out.println("Hiệu của 2 số: " + (x-y));break;
            case "*": System.out.println("Tích của 2 số: " + (x*y));break;
            case "/": System.out.println("Thương của 2 số: " + (x/y));break;
            
        }          
    } 
}

class Calculator3{
    // Good. Nhưng không cần phải tách lớp thế này nhé. 
    //  E giải thích lý do sao em lại truyền nguyên cục Integer này vào?
    /// Nhớ rằng constructor củng là hàm nhưng là hàm đặt biệt. 
    // không nên viết hàm như thế này để 
    public Calculator3(Integer a,Integer b){
        System.out.println("    Kết quả câu 3: ");
        System.out.println("a^2+b = " + (Math.pow(a,2) +b));
    }
}

class Calculator4{
       // Good. Nhưng không cần phải tách lớp thế này nhé. 
         //E giải thích lý do sao em lại truyền nguyên cục Integer này vào
         // không nên viết hàm như thế này để tính
         //kết quả của phép chia int thì e biết rồi như e đã ép kiểu sang double vậy sao không thể lấy phần thập phân?
    public Calculator4(Integer a, Integer b){        
        System.out.println("    Kết quả câu 4: ");
        System.out.println("a / b = " + ((double)a/(double)b)  + " Không thể lấy phần thập phân!");
    }
    
}

class Calculator5{
    // Good. Nhưng không cần phải tách lớp thế này nhé. 
         //E giải thích lý do sao em lại truyền nguyên cục Integer này vào
         // không nên viết hàm như thế này để tính
    public Calculator5(Double x, Integer y){
        System.out.println("    Kết quả câu 5: ");
        System.out.println("Tổng của 2 số: " + (x+y));
        System.out.println("Hiệu của 2 số: " + (x-y));
        System.out.println("Tích của 2 số: " + (x*y));
        System.out.println("Thương của 2 số: " + (x/y));
    }
}

class ChangeConst6{
    // hằng số là không thay đổi đc nhé
    int x;
    public ChangeConst6(int x){
        this.x = x;
    }
    public int getX() {return x;}
    public void setX(int newX) {x = newX;}
}

class InputOutput8{
    // anh giải thíhc câu này rồi. nó là biến tham chiếu gioosng như ngày xưa học c++ v em . 
    // A có giải thích thằng này rồi nhé.
    public InputOutput8(){
        Scanner input = new Scanner(System.in);
        System.out.println("    Kết quả câu 8: ");
        System.out.println("Vui lòng nhập giá trị số nguyên:");
        int value = input.nextInt();
        int valueLater = ++value;
        System.out.println("Giá trị giá nguyên vừa nhập là  " + --value + ", sau khi thay đổi là: " + valueLater);
    }
}

class ConvertValue10{
    // anh có giải thích rồi nhé 
    public ConvertValue10(int x, double y){
        System.out.println("    Kết quả câu 9: ");
        double xD = (double)x;
        x = (int)xD;
        int yI = (int)y;
        y = (double)yI;
        System.out.println("Ép kiểu x: " + xD + " " + x + " và y: " + yI + " " + y);
    }
}

interface Hello{
    public String sayHello(String name,String com);
}

// em tách lớp ra thế này rất tốt. Nhưng củng không nên gộp chung lại trong 1 file. 
// em nên tách ra thành từng file cho từng lớp. 
// Em sửa lại chỉ cần viết hàm bình thường thôi nhé! Vì đề chỉ yêu cầu là viết hàm nhé.
public class App {
    public static void main(String[] args) throws Exception {
        /*
        new Hi1();
        new Calculator2(2.5, 1.9,"*");
        new Calculator3(187,296);
        new Calculator4(1, 10);
        new Calculator5(2.5, 100);
        new InputOutput8();
        new ConvertValue10(296, 18.7);
        Hello s = (name,com) -> "Hello, " +name + ". Welcome to " + com;
        System.out.println(s.sayHello("Lam", "CPS"));
        */
        
        
    }
}