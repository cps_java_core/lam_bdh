public class Cau4_TimCacMaxMin{
       static int serial =0;
       static double max = Cau3_TimMaxMin.TimMax();
       static double min = Cau3_TimMaxMin.TimMin();
       static double[] arrayLook = Cau3_TimMaxMin.array;
       static double[] maxArray = new double[arrayLook.length];
       static double[] minArray = new double[arrayLook.length];

    public static double[] Maxs(){
        for(int i = 0;i<maxArray.length;i++){
           maxArray[i] = -1;
        }

        for(int i = 0; i< arrayLook.length;i++){
            if(arrayLook[i] == max){  
                
                maxArray[serial] = i+1;
                serial++;
            }
            
        }
        return maxArray;
    }

    public static double[] Mins(){
        for(int i = 0;i<minArray.length;i++){
           minArray[i] = -1;
        }

        for(int i = 0; i< arrayLook.length;i++){
            if(arrayLook[i] == min){  
                
                minArray[serial] = i+1;
                serial++;
            }
            
        }
        return minArray;
    }

    public static void Show(){
        System.out.println("\nGía trị lớn nhất của mảng là: " + max + " tại các vị trí: ");
        for(double item : Maxs()){
            if(item != -1)
            System.out.print(item + "\t");
        }
        System.out.println("\nGía trị nhỏ nhất của mảng là: " + min +" tại các vị trí: ");
        for(double item : Mins()){
            if(item != -1)
            System.out.print(item + "\t");
        }
    }
}