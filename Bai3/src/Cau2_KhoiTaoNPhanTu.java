import java.util.Scanner;

public class Cau2_KhoiTaoNPhanTu{
    public static double[] MangGiaTri(){
        Scanner sc  = new Scanner(System.in);
        int n = Cau1_NhapSoNguyenDuong.NhapSo();
        double []array = new double[n];
        System.out.println("Vui lòng nhập giá trị cho các phần tử của mảng n:");
        for(int i = 0; i< n;i++){
            System.out.println("Giá trị của phần tử thứ " + (i+1) + " là");
            array[i] = sc.nextDouble();
        }
        System.out.println("Các giá trị trong mảng n là: ");
        for(double arrayChil : array){
            System.out.print(arrayChil + "\t");
        }
        return array;
    }
}