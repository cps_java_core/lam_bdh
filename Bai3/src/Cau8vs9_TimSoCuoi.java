public class Cau8vs9_TimSoCuoi{

    

    public static int LastEven(int []arr){
        int le = 0;
        for(int i = arr.length-1;i>1;i--){
            if(arr[i] %2 ==0)
            {
                le = arr[i];
                break;
            }
        }
        return le;
    }

    public static int LastOdd(int []arr){
        int lo = 0;
        for(int i = arr.length-1;i>1;i--){
            if(arr[i] %2 ==1)
            {
                lo = arr[i];
                break;
            }
        }
        return lo;
    }

    public static void Show(){
        int[] array = Cau5_MangNgauNhien.TaoMang();
        System.out.println("Giá trị của mảng là: ");
        for(int item: array){
            System.out.print(item + "\t");
        }
        System.out.println("\nSố nguyên chẵn cuối cùng"+LastEven(array));
        System.out.println("Số nguyên lẻ cuối cùng" + LastOdd(array));
    }
}