import java.util.Random;

public class Cau5_MangNgauNhien{
    static int n = Cau1_NhapSoNguyenDuong.NhapSo();
    public static int[] TaoMang(){
        
        int[] array = new int[n];
        Random rand = new Random();
        for(int i = 0;i<n;i++){
            array[i] = rand.nextInt(n*10+1);
        }
        return array;
    }

    public static int SumEven(int []array){
        int sum =0;
        for(int item : array){
            if(item%2==0){
                sum+=item;
            }
        }
        return sum;
    }

    public static int SumOdd(int []array){
        int sum =0;
        for(int item : array){
            if(item%2==1){
                sum+=item;
            }
        }
        return sum;
    } 

    public static int[] EvenNumber(int []array){
        int[] evenNumbers = new int[n];
        for(int a =0;a<n;a++){
            evenNumbers[a] = 1;
        }
        int i=0;
        for(int item : array){
            if(item%2==0){
                evenNumbers[i] = item;
                i++;
            }
        }
        return evenNumbers;
    }

    public static int[] OddNumbers(int []array){
        int[] oddNumbers = new int[n];
        for(int a =0;a<n;a++){
            oddNumbers[a] = 0;
        }
        int i=0;
        for(int item : array){
            if(item%2==1){
                oddNumbers[i] = item;
                i++;
            }
        }
        return oddNumbers;
    }

    public static void Show(){
        int []array = TaoMang();
        System.out.println("Giá trị của mảng là: ");
        for(int item: array){
            System.out.print(item + "\t");
        }
        System.out.println("\nCác phần tử là số chẵn: ");
        for(int item : EvenNumber(array)){
            if(item!=1){
                System.out.print(item + "\t");
            }
        }
        System.out.println("\nCác phần tử là số lẻ: ");
        for(int item : OddNumbers(array)){
            if(item!=0){
                System.out.print(item + "\t");
            }
        }
        System.out.print("\nTổng giá trị các phần tử là số chẵn: "+SumEven(array));;
        System.out.println("\nTổng giá trị các phần tử là số lẻ: "+SumOdd(array));;
    }
}