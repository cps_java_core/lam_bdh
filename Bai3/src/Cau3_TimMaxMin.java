public class Cau3_TimMaxMin {
    static double[] array = Cau2_KhoiTaoNPhanTu.MangGiaTri();
    static double max = array[0],min = array[0];

    public static double TimMax(){
        
        for(double arrayChil: array){
            if(arrayChil > max)
            max = arrayChil;
        }
        
        return max;
    }

    public static double TimMin(){
               
        for(double arrayChil:array){
            if(arrayChil < min)
            min = arrayChil;
        }
       
        return min;
    }
    public static void Show(){
        max = TimMax();
        min = TimMin();
        System.out.println("\nGía trị lớn nhất của mảng là: " + max);
        System.out.println("Gía trị nhỏ nhất của mảng là: " + min);
    }
}