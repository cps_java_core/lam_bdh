import java.util.Scanner;

public class Cau1_NhapSoNguyenDuong {
    public static int NhapSo(){
        int i;
        Scanner sc = new Scanner(System.in);
        
        while(true){
            System.out.println("Nhập 1 số nguyên dương!");
            i = sc.nextInt();
            if(i>0)
            break;
            else
            System.out.println("Nhập sai, vui lòng nhập lại 1 số nguyên dương!");
        }
        
        System.out.println("Số nguyên dương vừa nhập là:");
        System.out.println(i);
        return i;
    }

}