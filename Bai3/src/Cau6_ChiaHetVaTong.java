import java.util.Random;

public class Cau6_ChiaHetVaTong {
    public Cau6_ChiaHetVaTong(){
        Random rand = new Random();
        int[]array = Cau5_MangNgauNhien.TaoMang();
        System.out.println("Giá trị của mảng là: ");
        for(int items: array){
            System.out.print(items + "\t");
        }
        int[]result = new int[Cau5_MangNgauNhien.n];
        int i = 0,sum=0;
        for(int item : array){
            if(item%10==0){
                result[i] = item;
                i++;
            }
        }
        System.out.println("\nGiá trị thỏa chia hết cho 5 và 2 là: ");
        for(int item:result){
            if(item!= 0){
                sum+=item;
                System.out.print(item + "\t");
            } 
        }
        
        System.out.println("\nTổng: "+sum);
    }
}