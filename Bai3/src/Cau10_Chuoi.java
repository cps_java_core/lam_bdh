import java.util.Scanner;

public class Cau10_Chuoi{
    public static String[] TaoChuoi(){
        int n = Cau1_NhapSoNguyenDuong.NhapSo();
        Scanner sc = new Scanner(System.in);
        String[]array  = new String[n];
        for(int i = 0;i<n;i++){
            System.out.println("Nhập giá trị chuỗi cho phần tử mảng thứ " + (i+1)+": ");
            array[i] = sc.next();
        }
        return array;
    }

    public static void Show(){
        String[] arr = TaoChuoi();
        for(int i = 0;i<arr.length;i++){
            System.out.println((i+1)+ ": " + arr[i] + ": độ đài là "+ arr[i].length());
        }
    }
}